﻿## Analysis of a rotating disc with updated geometry

###  Description

This test consists of a single disc that is rotating about the z-axis. Due to the rotation the ALE-specific convective terms are necessary to keep to transport the fluid back to to its original position (countermoving to the imprinted rotation). On the boundary a simple pressure function is prescribed leading to a wave propagating inwards. If everything is working correctly, the results should be similar to one obtained with a non-rotating case.


