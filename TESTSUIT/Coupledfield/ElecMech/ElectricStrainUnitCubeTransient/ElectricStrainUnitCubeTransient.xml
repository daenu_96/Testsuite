<cfsSimulation xmlns="http://www.cfs++.org/simulation"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.cfs++.org/simulation 
    ../../../../../share/xml/CFS-Simulation/CFS.xsd">
    
    <documentation>
        <title></title>
        <authors>
            <author>apapst</author>
        </authors>
        <date>2022-03-10</date>
        <keywords>
            <keyword>electrostatic</keyword>
            <keyword>piezo</keyword>
            <keyword>mechanic</keyword>
        </keywords>
        <references></references>
        <isVerified>no</isVerified>
        <description>
            Simple unitcube where potential-difference gets applied.
            This leads to an electric field, which is then used for forwardcoupling 
            towards the mechanic problem via "electric Strain" (similar as thermal strain).
            
            This testcase covers:
                - Transient analysis:
                    - with hysteretic material behaviour
        </description>
    </documentation>  
    
	<fileFormats>
		<input>
			<hdf5 fileName="ElectricStrainUnitCubeTransient.h5ref"/>
		</input>
		<output>
			<hdf5 />
			<text id="txt" />
		</output>
		<materialData file="mat.xml" format="xml" />
	</fileFormats>
	<domain geometryType="3d">
		<regionList>
			<region name="Vol" material="PZT-4" />
		</regionList>
	</domain>
	<!-- ========================= -->
	<!-- transient case with hysteresis behavior -->
	<!-- ========================= -->
	<sequenceStep index="1">
		<analysis>
			<transient>
				<numSteps>128</numSteps>
				<deltaT>0.1</deltaT>
				<allowPostProc>yes</allowPostProc>
			</transient>
		</analysis>
		<pdeList>
			<electrostatic>
				<regionList>
					<region name="Vol" nonLinIds="perm" />
				</regionList>
				<nonLinList>
					<elecPermittivity id="perm"
						model="JilesAthertonModel" />
				</nonLinList>
				<bcsAndLoads>
					<potential name="S_B" value="(t lt 6.2831853071795865e5)? 1e5*t/6.283185307179586 : 5e5 - 4e5*cos(t)" />
					<ground name="S_T" />
				</bcsAndLoads>
				<storeResults>
					<elemResult type="elecFieldIntensity">
						<allRegions />
					</elemResult>
					<elemResult type="elecFluxDensity">
						<allRegions />
					</elemResult>
				</storeResults>
			</electrostatic>
		</pdeList>
		<linearSystems>
			<system>
				<solutionStrategy>
					<standard>
						<nonLinear logging="yes">
							<lineSearch type="none" />
							<incStopCrit>1e-3</incStopCrit>
							<resStopCrit>1e-3</resStopCrit>
							<maxNumIters>200</maxNumIters>
						</nonLinear>
					</standard>
				</solutionStrategy>
			</system>
		</linearSystems>
	</sequenceStep>
	<sequenceStep index="2">
		<analysis>
			<transient initialTime="zero">
				<numSteps>128</numSteps>
				<deltaT>0.1</deltaT>
			</transient>
		</analysis>
		<pdeList>
			<mechanic subType="3d">
				<regionList>
					<region name="Vol" />
				</regionList>
					<bcsAndLoads>
						<fix name="P_NWB">
							<comp dof="z" />
						</fix>
						<fix name="P_SEB">
							<comp dof="y" />
							<comp dof="z" />
						</fix>
						<fix name="P_SWB">
							<comp dof="x" />	
							<comp dof="y" />
							<comp dof="z" />
						</fix>
						<elecStrain name="Vol">
							<excitation name="Vol">
								<sequenceStep index="1">
									<quantity name="elecFieldIntensity" pdeName="electrostatic"/>
									<timeFreqMapping>
										<continuous interpolation="linear"/>
									</timeFreqMapping>
								</sequenceStep>
							</excitation>
							<direction name="Vol">
								<sequenceStep index="1">
									<quantity name="elecFieldIntensity" pdeName="electrostatic"/>
									<timeFreqMapping>
										<continuous interpolation="linear"/>
									</timeFreqMapping>
								</sequenceStep>
							</direction>
							<scaling>
								<dependency name="Vol">
									<sequenceStep index="1">
										<quantity name="elecFluxDensity" pdeName="electrostatic"/>
										<timeFreqMapping>
											<continuous interpolation="linear"/>
										</timeFreqMapping>
									</sequenceStep>
								</dependency>
								<formulation>
									<linear>
										<factor>
										2
										</factor>
									</linear>
								</formulation>
							</scaling>
						</elecStrain>
				</bcsAndLoads>
				<storeResults>
					<nodeResult type="mechDisplacement">
						<allRegions />
					</nodeResult>
				</storeResults>
			</mechanic>
		</pdeList>
	</sequenceStep>
</cfsSimulation>
