<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  
  <documentation>
    <title>Vibrating Eddy Current Plate with Fluid</title>
    <authors>
      <author>ahauck</author>
    </authors>
    <date>2013-02-28</date>
    <keywords>
      <keyword>magneto-mechanic</keyword>
      <keyword>mechanic-acoustic</keyword>
      <keyword>abc</keyword>
    </keywords>
    <references>
      n.a.
    </references>
    <isVerified>no</isVerified>
    <description>
      This is a plane setup of a coil, fed by alternating current.
      It generates eddy-curtens in a clamped aluminum beam, which vibrates
      due to Lorentz forces, leading to acoustic wave propagation into the
      neighboring fluid domain (air).
      
      This model has no analytical reference, but serves as a proof-of-concept
      simulation of a coupled magneto-mechanical-acoustic system.
    </description>
  </documentation>
  
 <fileFormats>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="air" material="AIR"/>
      <region name="coil" material="AIR"/>
      <region name="plate" material="alu"/>
      <region name="fluid" material="AIR"/>
    </regionList>
  </domain>

  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>20</numSteps>
        <deltaT>2e-05</deltaT>
      </transient>
    </analysis>

    <pdeList>
    <magnetic  systemId="mag">
        <regionList>
          <region name="air"/>
          <region name="coil"/>
          <region name="plate"/>
          <region name="fluid"/>
        </regionList>

        <bcsAndLoads>
          <fluxParallel name="pot">
            <comp dof="z"/>
          </fluxParallel>
        </bcsAndLoads>
        
	<coilList>
           <coil id="myCoil">
            <source type="current" value="1*sin(2*1e3*pi*t)"/>
              <part>
                <regionList>
                  <region name="coil"/>
                </regionList>
		<direction>
		  <analytic/>
		</direction>
                <wireCrossSection area="2e-7"/>
                <resistance value="0"/> 
              </part>
            </coil>
          </coilList>
        
        <storeResults>
          <nodeResult type="magPotential">
            <allRegions/>
          </nodeResult>
          <elemResult type="magFluxDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="magEddyCurrentDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="magForceLorentzDensity">
            <regionList>
              <region name="plate"/>
            </regionList>
          </elemResult>
          <regionResult type="magForceLorentz">
            <allRegions/>
          </regionResult>
        </storeResults>
      </magnetic>
      
     <mechanic subType="planeStrain" systemId="mech">
        <regionList>
          <region name="plate" dampingId="r"/>
        </regionList>
        <dampingList>
          <rayleigh id="r"/>
        </dampingList>
        <bcsAndLoads>
          <fix name="fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          
          <fix name="sym">
            <comp dof="x"/>
          </fix>

          <!-- Coupling to magnetic PDE -->
          <forceDensity name="plate">
            <coupling pdeName="magnetic">
              <quantity name="magForceLorentzDensity"/>
            </coupling>
          </forceDensity>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
          <nodeResult type="mechVelocity">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
      
      <acoustic formulation="acouPotential" systemId="mech">
        <regionList>
          <region name="fluid"/>
        </regionList>
	<bcsAndLoads>
	  <absorbingBCs name="abc" volumeRegion="fluid"/>
	</bcsAndLoads>
        <storeResults>
          <nodeResult type="acouPotentialD1">
            <allRegions/>
          </nodeResult>
          <elemResult type="acouPressure">
            <allRegions/>
          </elemResult>
          <elemResult type="acouVelocity">
            <allRegions/>
          </elemResult>
        </storeResults>
      </acoustic>
    </pdeList>
    
    <couplingList>
      <iterative>
        <convergence logging="yes" maxNumIters="1" stopOnDivergence="no"/>
      </iterative>
        <direct>
        <acouMechDirect>
          <surfRegionList>
            <surfRegion name="coupling"/>
          </surfRegionList>
	</acouMechDirect>
      </direct>
    </couplingList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>

 </sequenceStep>
</cfsSimulation>
