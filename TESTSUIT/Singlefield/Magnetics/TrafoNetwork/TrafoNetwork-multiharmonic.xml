<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <documentation>
        <title>Magnetic Network defining a simple transformator with resistive load</title>
        <authors>
            <author>ftoth</author>
        </authors>
        <date>2018-09-19</date>
        <keywords>
            <keyword>magneticEdge</keyword>
        </keywords>
        <references> </references>
        <isVerified>no</isVerified>
        <description> 
Simple magnetic network of a conducting wire though a magnetic core.
The BCs are set such that magnetic flux is possible only in x-direction.
The wire is excited by current in z-direction.

We selcect the geometry such that we have unit-areas and lengths.

For a comparison with the analysic solution see the iPython notebook.
        </description>
    </documentation>
    <fileFormats>
        <input>
            <!--<hdf5 fileName="NetworkHeat_HarmonicIterative.h5ref"/>-->
            <gmsh fileName="SimpleTrafo.msh"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>
    <domain geometryType="3d" printGridInfo="yes">
        <variableList>
            <var name="a" value="1.0"/>
            <var name="b1" value="2.0"/>
            <var name="b2" value="1.0"/>
            <var name="b3" value="b2"/>
            <var name="b4" value="b1"/>
            <var name="c" value="1.0"/>
            <var name="I_coil" value="ICOIL"/>
            <var name="f_coil" value="FCOIL"/>
        </variableList>
        <regionList>
            <region name="V1" material="MAT"/>
            <region name="V2" material="Resistor"/>
            <region name="V3" material="Coil"/>
            <region name="V4" material="MAT"/>
        </regionList>
        <surfRegionList> </surfRegionList>
    </domain>
    
    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>0</isoOrder>
        </Legendre>
    </fePolynomialList>
    
    <sequenceStep index="1">
        <analysis>
            <!--<static/>-->
            <multiharmonic>
                <baseFreq>f_coil</baseFreq>
                <numHarmonics_N>NHARMONICS</numHarmonics_N>
                <numFFTPoints>1024</numFFTPoints>
                <fullSystem>false</fullSystem>
            </multiharmonic>
        </analysis>
        
        <pdeList>
            <magneticEdge>
                <regionList>
                    <region name="V1" polyId="Hcurl" nonLinIds="perm"/>
                    <region name="V2" polyId="Hcurl"/>
                    <region name="V3" polyId="Hcurl"/>
                    <region name="V4" polyId="Hcurl" nonLinIds="perm"/>
                </regionList>
                
                <nonLinList>
                    <permeability id="perm"/>
                </nonLinList>
                
                <bcsAndLoads>
                    <fluxParallel name="S_T"/>
                    <fluxParallel name="S_B"/>
                    <fluxParallel name="S_N"/>
                    <fluxParallel name="S_S"/>
                </bcsAndLoads>
                
                <coilList>
                    <coil id="coil">
                        <sourceMultiharmonic type="current">
                            <harmonic harmonic="1" value="I_coil"/><!-- *sqrt(1-pi*pi*f*f)/(1+pi*pi*f*f)  -->
                        </sourceMultiharmonic>
<!--                        <source type="current" value="I_coil"/><!-\- *sqrt(1-pi*pi*f*f)/(1+pi*pi*f*f)  -\->-->
                        <part id="1">
                            <regionList>
                                <region name="V3"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="z" value="1"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="b3*a"/>
                        </part>
                    </coil>
                </coilList>
                
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magCoilCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotentialD1">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magJouleLossPowerDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magFieldIntensity">
                        <allRegions/>
                    </elemResult>   
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>  
                    <surfRegionResult type="magFlux">
                        <surfRegionList>
                            <surfRegion name="S1_W" outputIds="txt"/>
                        </surfRegionList>
                    </surfRegionResult>
                    <coilResult type="coilInducedVoltage">
                        <coilList>
                            <coil id="coil" outputIds="txt"/>
                        </coilList>
                    </coilResult>
                </storeResults>
            </magneticEdge>
        </pdeList>
        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <!--<exportLinSys mass="true" stiffness="true" system="true"/>-->
                        <matrix reordering="noReordering" storage="sparseNonSym"/>
                        <nonLinear method="fixPoint">
                            <lineSearch/>
                            <incStopCrit>1e-2</incStopCrit>
                            <resStopCrit>1e-2</resStopCrit>
                            <maxNumIters>20</maxNumIters>
                        </nonLinear>
                    </standard>
                </solutionStrategy>
                <solverList>
                    <pardiso id="pardiso"/>
                    <gmres>
                        <stoppingRule type="absNorm"/>
                        <tol>1e-16</tol>
                        <maxIter>15</maxIter>
                        <maxKrylovDim>NHARMONICS</maxKrylovDim>
                        <logging>yes</logging>
                        <consoleConvergence>yes</consoleConvergence>
                    </gmres>
                </solverList>
                <precondList>
                    <SBMDiag>
                        <precond block="1" id="def"/>
                        <precond block="2" id="def"/>
                        <precond block="3" id="def"/>
                        <precond block="4" id="def"/>
                        <precond block="5" id="def"/>
                        <precond block="6" id="def"/>
                        <precond block="7" id="def"/>
                        <precond block="8" id="def"/>
                        <precond block="9" id="def"/>
                        <precond block="10" id="def"/>
                        <precond block="11" id="def"/>
                        <precond block="12" id="def"/>
                        <precond block="13" id="def"/>
                        <precond block="14" id="def"/>
                        <precond block="15" id="def"/>
                        <precond block="16" id="def"/>
                        <precond block="17" id="def"/>
                        <precond block="18" id="def"/>
                        <precond block="19" id="def"/>
                        <precond block="20" id="def"/>
                        <precond block="21" id="def"/>
                        <precond block="22" id="def"/>
                        <precond block="23" id="def"/>
                        <precond block="24" id="def"/>
                        <precond block="25" id="def"/>
                        <precond block="26" id="def"/>
                    </SBMDiag>
                    <pardiso id="def"/>
                </precondList>
            </system>
        </linearSystems>
    </sequenceStep>
</cfsSimulation>
